import { ethers } from 'hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { shouldVerifyContract } from 'utils/deploy';

const deployFunction: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployer } = await hre.getNamedAccounts();
  const currentNonce: number = await ethers.provider.getTransactionCount(deployer);

  
  const reactorfactory = await hre.deployments.deploy('ReactorFactory', {
    contract: 'solidity/contracts/ReactorFactory.sol:ReactorFactory',
    from: deployer,
    //args: cknftHelperArgs,
    log: true,
  });

  if (hre.network.name !== 'hardhat' && (await shouldVerifyContract(reactorfactory))) {
    await hre.run('verify:verify', {
      address: reactorfactory.address,
      //constructorArguments: cknftHelperArgs,
    });
  }

};

deployFunction.tags = ['ReactorFactory', 'testnet'];

export default deployFunction;
