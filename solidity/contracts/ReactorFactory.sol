// SPDX-License-Identifier: MIT LICENSE

pragma solidity ^0.8.14;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

import "./Reactor.sol";


contract ReactorFactory {
    Reactor[] public deployedReactors;   
    event ReactorCreated(address _creator, address _reactor);

    address public owner;
    address comiteFiducia = 0x24735847b2879121648Ec54483422c7c78fC4Dfd; // address de fees
    address public admin = 0xb28d1F1B8b0ad89aF30cb20E9a4C234Bf0D98005; // address del admin (Titanius)

    mapping(address => bool) public allowlistsAddresses;

    modifier onlyAdmin() {
        require(msg.sender == admin, "no eres el admin");
        _;
    }   

    function allowlistsUsers(address[] calldata _users) public onlyAdmin {
        for (uint256 i = 0; i < _users.length; i++) {
            allowlistsAddresses[_users[i]] = true;
        }
    }


    function createReactor (
        string memory _proyectDescription,
        string memory _proyectName,
        string memory _newuri,
        uint256 _cantidadSupply,
        uint256 _tokenPrices        
        
    ) public returns (address) {

        require(
            allowlistsAddresses[msg.sender] == true,
            "no estas en la lista de autorizados!"
        );

        Reactor newReactor = new Reactor(
            _proyectDescription,
            _proyectName,
            _newuri,
            _cantidadSupply,
            _tokenPrices, // considerar los decimales de USDT (18 decimales)
            payable(owner = msg.sender),
            payable(comiteFiducia)
        );
        emit ReactorCreated(msg.sender, address(newReactor));
        deployedReactors.push(newReactor);
        return address(newReactor);
    }

    function getDeployedReactors() public view returns (Reactor[] memory) {
        return deployedReactors;
    }
}
