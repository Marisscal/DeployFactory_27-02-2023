// SPDX-License-Identifier: MIT LICENSE

pragma solidity ^0.8.14;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/security/Pausable.sol";


/*

* Reactor

* Version 0.0.1 Hernan Abeldaño

* November-04-2022

* Copyright © Titanius tech corp.

* All rights reserved

*/

interface ICOIN {
    
    /**
     * @dev funciones necesarias para poder implementar distintas
     * stables coins, siempre basadas en ERC20.     
     */
    event Transfer(address indexed from, address indexed to, uint256 value);    
    
    event Approval(address indexed owner, address indexed spender, uint256 value);

    /**
     * @dev retorna la cantidad total de tokens existentes.
     */
    function totalSupply() external view returns (uint256);    
    
    function balanceOf(address account) external view returns (uint256);
    
    /**
     * @dev mueve un `amount` de tokens desde la cuenta del caller hacia la address `to`.        
     */
    function transfer(address to, uint256 amount) external returns (bool);    
   
    function allowance(address owner, address spender) external view returns (uint256);    
    
    function approve(address spender, uint256 amount) external returns (bool);
    
    /**
     * @dev mueve una cantidad `amount` de tokens desde el `from` hacia el `to` usando el mecanismo de 
     * allowance. `amount` is then deducted from the caller's
     * allowance.     
     */
    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) external returns (bool);
}


contract Reactor is ERC1155, Pausable {


    string public contractUri;

    uint256 public constant MAX_TOKEN_ID_PLUS_ONE = 1; // solo un ids, empezando por el 0
    uint256 public SC_TIMER;
    uint256 public max_mint_tx = 50;
    uint256 public MAX_MINT_TOTAL = 50;   

    
    
    // Mapping de los balances de ID por accounts
    mapping(uint256 => mapping(address => uint256)) private _balances;
    mapping(uint256 => uint256) public tokenIdToExistingSupply;
    mapping(uint256 => uint256) public tokenIdToMaxSupplyPlusOne; // seteo en el constructor    
    mapping(address => uint256) public nftPermit;    

    bool private _reentrant = false;
    bool public isContractActive = false;

    modifier nonReentrant() {
        require(!_reentrant, "No reentrancy");
        _reentrant = true;
        _;
        _reentrant = false;
    }

    address payable public owner;
    address public admin = 0xb28d1F1B8b0ad89aF30cb20E9a4C234Bf0D98005; // address del admin (Titanius)
    address payable public comiteFiducia; // seteada desde el Factory
    address public stable; // address del contrato de la StableCoin (tener en cuenta los decimals)

    string public description;
    string public name;
    string public _uri;
    //uint256 public tokenSupply;
    uint256 public tokenPrices;
    mapping(address => mapping(uint256 => uint256)) private _orderBook;

    event Order(
        address indexed _customer,
        uint256 indexed _id,
        uint256 indexed _amount
    );

    event OrderBatch(
        address indexed _customer,
        uint256[] indexed _ids,
        uint256[] indexed _amounts
    );

    modifier onlyOwner() {
        require(msg.sender == owner, "no eres el owner");
        _;
    }  

    modifier onlyAdmin() {
        require(msg.sender == admin, "no eres el admin");
        _;
    }  

    constructor (
        string memory _proyectDescription,
        string memory _proyectName,
        string memory _newURI, 
        uint256  _cantidadSupply,
        uint256  _tokenPrices,
        address payable _deployerAddress,
        address payable _addressFiducia
    ) public ERC1155(_newURI) {        
        description = _proyectDescription;
        name = _proyectName;
        tokenIdToMaxSupplyPlusOne[0] = _cantidadSupply;
        tokenPrices = _tokenPrices;
        comiteFiducia = _addressFiducia;
        owner = _deployerAddress;

        contractUri = "ipfs//"; // contrato json de metadata para perfil de OpenSea
        
        Pausable._pause(); // el contrato se deploya sin pausa
        
        uint256[] memory _ids = new uint256[](1);
        uint256[] memory _amounts = new uint256[](1);
        for (uint256 i = 0; i < MAX_TOKEN_ID_PLUS_ONE; ++i) {
            _ids[i] = i;
            _amounts[i] = 1;
            tokenIdToExistingSupply[i] = 1;
        }
        _mintBatch(comiteFiducia, _ids, _amounts, ""); // se mintea de entrada 1 NFT de prueba
    }

    /// @dev function para OpenSea (en caso de aplicar) 
    function totalSupply(uint256 _id) external view returns (uint256) {
        require(_id < MAX_TOKEN_ID_PLUS_ONE, "id debe ser menor a 1");
        return tokenIdToExistingSupply[_id];
    }

    /// @dev function para OpenSea que retorna el uri de la metadata del Smart Contract
    function contractURI() external view returns (string memory) {
        return contractUri; // Lo lee OpenSea
    }

    /**
     * @dev 
     *
     * Requirements:
     * - se realiza el mapeo correspondiente y primero se llama con el sender
     * - luego se indica address con id especificos
     * - `account` no puede ser la address dead.
     */

     function getBalanceTokenId(uint256 _id)
        public
        view
        returns (uint256)
    {
        return ERC1155(address(this)).balanceOf(msg.sender, _id);
    }

    function balanceTokenId(address account, uint256 id) public view virtual returns (uint256) {
        require(account != address(0), "ERC1155: la address Dead no es un owner valido");
        return balanceOf(account, id);
    }

    /**
     * @dev ver {IERC1155}.
     *
     * Requirements:
     *
     * - `account` e `ids` deben tener la misma longitud.
     */
    function balanceOfBatchIds(address[] memory accounts, uint256[] memory ids)
        public
        view
        virtual        
        returns (uint256[] memory)
    {
        require(accounts.length == ids.length, "ERC1155: longitudes de accounts e ids deben coincidir");

        uint256[] memory batchBalances = new uint256[](accounts.length);

        for (uint256 i = 0; i < accounts.length; ++i) {
            batchBalances[i] = balanceOf(accounts[i], ids[i]);
        }

        return batchBalances;
    }    

    // SETTERS

    function setFiducia(address _addressFiducia) external onlyAdmin {
        require(_addressFiducia != address(0), "no setear la wallet 0");
        comiteFiducia = payable(_addressFiducia);
    }

    function setContractURI(string calldata _newURI) external onlyOwner {
        contractUri = _newURI; // updatable para el marketplaces likeble de OpenSea
    }

    function setPaused(bool _paused) external onlyOwner {
        if (_paused) _pause();
        else _unpause();
    }

    function setContractActive(uint256 _date) external onlyOwner {
        SC_TIMER = block.timestamp + _date * 1 days;
        isContractActive = true;
    }

    function setStableCoin(address _stableCoin) public onlyOwner {
        stable = _stableCoin;
    }

    function setMaxALMint(uint256 _max) public onlyOwner {
        MAX_MINT_TOTAL = _max;
    }

    // PUBLIC FUNCTIONS

    // @notice Mints uno o mas acciones de Reactores, dependiendo del amount.
    // @dev Tenemos distintos limites de minteo.
    // @param para cada id de NFT (aacciones de Reactor) a mintear.
    // @param amount The amount of NFT to be minted. 

    function mint(uint256 _id, uint256 _amount)
        external
        nonReentrant
        whenNotPaused
    {
        require(
            (isContractActive),
            "el contrato no esta activo"
        );

        // en este caso, el Timer se activa por un plazo de 24h
        require(
            (SC_TIMER > block.timestamp),
            "el Contrato vencio"
        );
               
        require(
            nftPermit[msg.sender] + _amount <= MAX_MINT_TOTAL,
            "Solo se pueden mintear 50 Nfts acciones por wallet."
        );
        require(_id < MAX_TOKEN_ID_PLUS_ONE, "id debe ser menor a 1");
        require(_amount > 0 && _amount <= max_mint_tx, "monto invalido para mint");                

        uint256 existingSupply = tokenIdToExistingSupply[_id];
        require(
            existingSupply + _amount < tokenIdToMaxSupplyPlusOne[_id],
            "supply excedido"
        );
        
        require(msg.sender == tx.origin, "no smart contracts");

        ICOIN(stable).transferFrom(msg.sender, comiteFiducia, _amount * tokenPrices);    

        unchecked {
            existingSupply += _amount;
        }
        tokenIdToExistingSupply[_id] = existingSupply;      
        
        for (uint256 i = 0; i < _amount; i++) {
                        
            nftPermit[msg.sender]++;   
        }
        _mint(msg.sender, _id, _amount, "");
    }

    // @notice Solo el owner puede Mintear uno o mas acciones de Reactores, dependiendo del amount.
    // @dev Tenemos distintos limites de minteo.
    // @param para cada id de NFT (acciones de Reactor) a mintear.
    // @param amount The amount of NFT to be minted.  

    function ownerMint(
        address _to,
        uint256 _id,
        uint256 _amount
    ) external onlyOwner whenNotPaused {
        require(_id < MAX_TOKEN_ID_PLUS_ONE, "id debe ser menor a 1");

        uint256 existingSupply = tokenIdToExistingSupply[_id];
        require(
            existingSupply + _amount < tokenIdToMaxSupplyPlusOne[_id],
            "supply excedido"
        );
        unchecked {
            existingSupply += _amount;
        }
        tokenIdToExistingSupply[_id] = existingSupply;
        _mint(_to, _id, _amount, "");
    }

    // @notice Se puede Mintear uno o mas acciones de Reactores, de una o varias colecciones, dependiendo de los amount.
    // @dev Tenemos distintos limites de minteo.
    // @param para cada id de NFT (aacciones de Reactor) a mintear.
    // @param amount The amount of NFT to be minted. 

    function mintBatch(uint256[] calldata _ids, uint256[] calldata _amounts)
        external
        payable whenNotPaused
    {
        uint256 sumAmounts;
        uint256 arrayLength = _ids.length;
        for (uint256 i = 0; i < arrayLength; ++i) {
            sumAmounts += _amounts[i];
        }

        require(msg.value == sumAmounts * tokenPrices, "monto incorrecto de USDT");

        for (uint256 i = 0; i < arrayLength; ++i) {
            uint256 _id = _ids[i];
            uint256 _amount = _amounts[i];
            uint256 existingSupply = tokenIdToExistingSupply[_id];
            
            require(
                existingSupply + _amount < tokenIdToMaxSupplyPlusOne[_id],
                "supply excedido"
            );
            require(msg.sender == tx.origin, "no smart contracts");

            unchecked {
                existingSupply += _amount;
            }
            tokenIdToExistingSupply[_id] = existingSupply;
        }
        _mintBatch(msg.sender, _ids, _amounts, "");
    }    
        
    // ARRAYS

    function addToArray(uint256[] storage list, uint256 value) private {
        uint256 index = find(list, value);
        if (index == list.length) {
            list.push(value);
        }
    }

    function removeFromArray(uint256[] storage list, uint256 value) private {
        uint256 index = find(list, value);
        if (index < list.length) {
            list[index] = list[list.length - 1];
            list.pop();
        }
    }

    function find(uint256[] memory list, uint256 value) private pure returns(uint)  {
        for (uint i=0;i<list.length;i++) {
            if (list[i] == value) {
               return i;
            }
        }
        return list.length;
    }
    
}